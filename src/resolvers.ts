import usersData from "./data/users.json";
import postsData from "./data/posts.json";
import commentsData from "./data/comments.json";
import { sleep } from "./helpers";

let counter = 0;

export default {
  Query: {
    user: (root, args, ctx, info) => {
      return usersData.find(it => it.id === args.id);
    },
    counter: async () => {
      console.log("counter", Date.now());
      await sleep(1000);
      return counter;
    },
  },
  Mutation: {
    commentPost: (root, args, ctx, info) => {
      const { postId, text } = args.commentData;
      const { user } = ctx;

      if (!postsData.some(it => it.id === postId)) {
        return false;
      }

      commentsData.push({
        postId,
        text,
        authorId: user.id,
      });
      return true;
    },
    incrementCounter: async () => {
      console.log("increment", counter, Date.now());
      await sleep(1000);
      return ++counter;
    },
  },
  User: {
    posts: (user, args, ctx, info) => {
      return postsData.filter(it => it.authorId === user.id);
    },
    avatarUrl: (user, args, ctx, info) => {
      const resolution = args.size === "Big" ? "640x640" : "100x100";
      return `https://my-api.rest/avatar/${user.id}/${resolution}.jpg`;
    },
  },
  Post: {
    comments: (post, args, ctx, info) => {
      return commentsData
        .filter(it => it.postId === post.id)
        .slice(0, args.limit);
    },
    content: () => "Lorem ipsum dolor sit amet. ".repeat(100),
  },
  Comment: {
    author: (comment, args, ctx, info) => {
      return usersData.find(it => it.id === comment.authorId);
    },
  },
};
